from dataclasses import dataclass, field
import numpy as np

@dataclass
class TdParams:
    m50: float = 0.1
    slope: float = 0.2
    ms_pow: float = field(init=False)
    
    def __post_init__(self):
        self.ms_pow = self.m50**self.slope

def td(int, params:TdParams):
    ints_pow=np.power(int,params.slope)
    return ints_pow / (params.ms_pow + ints_pow)




