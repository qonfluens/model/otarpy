from dataclasses import dataclass
import numpy as np

from scipy.integrate import odeint
from scipy.interpolate import interp1d

@dataclass
class TkParams:
    ku: float = 0.1
    ke: float = 0.2
    

def tk_(ext, int_0, params:TkParams):
    return int_0 + params.ku * ext - params.ke * int_0

def tk(ext, int_0, params:TkParams):
    int = [0 for i in range(len(ext))]
    int[0] = tk_(ext[0], int_0, params)
    for i in range(len(ext)-1):
        int[i+1] = tk_(ext[i], int[i], params)
    return int

def dtk_dt_(y, t, fint, ku, ke):
    dtk = fint(t) *ku - ke * y
    return  dtk

def tk_ode(dose,int_0,tk0):
    t = np.linspace(0, len(dose), len(dose))
    fint = interp1d(t, dose)
    out = odeint(dtk_dt_, int_0, t[:-1], args=(fint, tk0.ku, tk0.ke))
    return out
