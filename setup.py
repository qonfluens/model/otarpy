from setuptools import setup

setup(
    name='otarpy',
    version='1.0.0',
    description='morse + python',
    packages=["otarpy"],
    install_requires=["numpy","scipy"],
)

